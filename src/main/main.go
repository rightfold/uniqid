package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"uniqid"
)

func main() {
	if len(os.Args) != 3 {
		fmt.Println("usage:", os.Args[0], "data-path", "http-address")
		os.Exit(1)
	}

	dataPath := os.Args[1]
	httpAddr := os.Args[2]

	server, err := uniqid.NewServer(dataPath)
	if err != nil {
		log.Fatalln(err)
	}

	httpServer := http.Server{
		Addr:    httpAddr,
		Handler: uniqid.NewHTTPHandler(server),
	}
	httpServer.ListenAndServe()
}
