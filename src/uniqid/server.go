package uniqid

import (
	"encoding/binary"
	"os"
	"sync"
)

type Server struct {
	file  *os.File
	mutex sync.Mutex
}

func NewServer(path string) (server *Server, err error) {
	file, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		return
	}

	offset, err := file.Seek(0, 2)
	if err != nil {
		return
	}

	if offset == 0 {
		var zero uint64 = 0
		err = binary.Write(file, binary.BigEndian, &zero)
		if err != nil {
			return
		}
	}

	server = &Server{file: file}
	return
}

func (server *Server) NewID() (id uint64, err error) {
	server.mutex.Lock()
	defer server.mutex.Unlock()

	_, err = server.file.Seek(0, 0)
	if err != nil {
		return
	}

	err = binary.Read(server.file, binary.BigEndian, &id)
	if err != nil {
		return
	}

	id++

	_, err = server.file.Seek(0, 0)
	if err != nil {
		return
	}

	err = binary.Write(server.file, binary.BigEndian, &id)
	return
}
