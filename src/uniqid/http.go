package uniqid

import (
	"net/http"
	"strconv"
)

func NewHTTPHandler(server *Server) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		id, err := server.NewID()
		if err != nil {
			http.Error(rw, err.Error(), 500)
			return
		}

		result := strconv.FormatUint(id, 10)
		rw.Write([]byte(result))
	})
}
